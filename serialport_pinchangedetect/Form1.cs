﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.IO.Ports;
using System.Threading;


namespace serialport_pinchangedetect
{
    public partial class Form1 : Form
    {
        volatile private bool continute = true;
        private Thread thread;
        volatile private int rtsCount = 0;
        public Form1()
        {
            InitializeComponent();
        }

        private void port_PinChanged(object sender, SerialPinChangedEventArgs e) // This will be called whenever the port changes.
        {
            string info = string.Empty;
            switch (e.EventType)
            {
                case SerialPinChange.CDChanged:
                    
                    break;
                case SerialPinChange.CtsChanged:
                    
                    break;
                case SerialPinChange.DsrChanged:
                    
                    break;
                case SerialPinChange.Ring:
                    rtsCount++;
                    
                    break;
                default:
                    break;
            }
        }

        private void run()
        {
            SerialPort port = new SerialPort("COM4", 9600, Parity.None, 8, StopBits.One); // Instantiate the communications // port with some basic settings
            
            port.PinChanged += new SerialPinChangedEventHandler(port_PinChanged); // New event trigger
            port.ReadTimeout = 10;
            port.WriteTimeout = 10;
            port.RtsEnable = true;
            port.Handshake = Handshake.None;
            port.Open(); // Open the port for communications

            byte[] sendBuf = new byte[1];
            byte[] recvBuf = new byte[1];

            sendBuf[0] = 0x55;
            while (continute == true)
            {
                try
                {
                    port.Write(sendBuf, 0, 1);

                    port.Read(recvBuf, 0, 1);
                }
                catch (TimeoutException)
                {
                    

                }
            }
            port.Close();
        }
        

        private void Form1_Load(object sender, EventArgs e)
        {

            thread = new Thread(run);
            thread.Start();
            
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            continute = false;
            thread.Join();
            Application.Exit();
        }
    }
}
